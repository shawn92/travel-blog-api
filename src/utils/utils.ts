import * as mongoose from "mongoose";

export function mapToKeys(docs: mongoose.Document[], key: string) {
    return docs.reduce((obj: any, item: any) => {
        obj[item[`${key}`]] = obj[item[`${key}`]] || [];
        obj[item[`${key}`]].push(item);
        return obj;
    }, {});
}

export function mapToObject(docs: mongoose.Document[], key: string) {
    return docs.reduce((obj: any, item: any) => {
        obj[item[`${key}`]] = item;
        return obj;
    }, {});
}