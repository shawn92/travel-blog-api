import * as express from "express";
import { json } from "body-parser";

import { DEV } from "./../config";
import { 
    PageRouter,
    PostRouter,
    MenuRouter,
    BannerRouter,
    UserRouter
} from "./routes";

export class App { 
    public server: express.Application;

    constructor() {
        this.server = express();

        this.setMiddleware();
        this.configureRoutes();
    }

    private setMiddleware() {
        this.server.use(json());

        if (DEV.CORS) {
            this.server.use(function(req, res, next) {
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
                res.header("Access-Control-Allow-Headers", "Content-Type, X-Auth");
                res.header("Access-Control-Expose-Headers", "X-Auth");
                next();
            }); 
        }
    }

    private configureRoutes() {
        this.server.use("/api", [ 
            new PageRouter().routes,
            new PostRouter().routes,
            new MenuRouter().routes,
            new BannerRouter().routes,
            new UserRouter().routes
        ]);
    }
}