export * from "./pages/page-router";
export * from "./posts/post-router";
export * from "./menus/menu-router";
export * from "./banners/banner-router";
export * from "./users/user-router";