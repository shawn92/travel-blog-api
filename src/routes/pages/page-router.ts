import { Router } from "express";

import { PageApiController } from "./../../controllers";
import { UserAuthentication } from "./../../security/user-authentication";

const router = Router();

export class PageRouter {
    private controller: PageApiController;
    private auth: UserAuthentication;

    constructor() {
        this.controller = new PageApiController();
        this.auth = new UserAuthentication();
    }

    get routes() {
        router.get("/pages", 
            this.controller.getAllPages
        );

        router.get("/pages/:slug", 
            this.controller.getPage
        );

        router.post("/pages", 
            this.auth.verifyHeaders,
            this.controller.addPage
        );

        router.patch("/pages/:slug",
            this.auth.verifyHeaders, 
            this.controller.updatePage
        );

        router.delete("/pages/:slug",
            this.auth.verifyHeaders,
            this.controller.deletePage
        );

        return router;
    }
}