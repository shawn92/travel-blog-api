import { Router } from "express";

import { MenuApiController } from "./../../controllers";
import { UserAuthentication } from "./../../security/user-authentication";

const router = Router();

export class MenuRouter {
    private controller: MenuApiController;
    private auth: UserAuthentication;

    constructor() {
        this.controller = new MenuApiController();
        this.auth = new UserAuthentication();
    }

    get routes() {
        router.get("/menus", 
            this.controller.getMenus
        );

        router.get("/menus/:slug", 
            this.controller.getMenus
        );

        router.post("/menus", 
            this.auth.verifyHeaders,
            this.controller.addMenu
        );

        router.patch("/menus/:slug", 
            this.auth.verifyHeaders,
            this.controller.updateMenu
        );

        router.delete("/menus/:slug", 
            this.auth.verifyHeaders,
            this.controller.deleteMenu
        );

        return router;
    }
}