import { Router } from "express";

import { UserApiController } from "./../../controllers";
import { UserAuthentication } from "./../../security/user-authentication";

const router = Router();

export class UserRouter {
    private controller: UserApiController;
    private auth: UserAuthentication;

    constructor() {
        this.controller = new UserApiController();
        this.auth = new UserAuthentication();
    }

    get routes() {
        const controller = this.controller;
        const auth = this.auth;

        router.post("/users",
            //auth.verifyHeaders,
            controller.createUser
        );

        router.post("/users/login",
            controller.loginUser
        );

        router.delete("/users/logout",
            auth.verifyHeaders,
            controller.logoutUser
        );

        return router;
    }
}