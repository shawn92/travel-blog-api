import { Router } from "express";

import { PostApiController } from "./../../controllers";
import { UserAuthentication } from "./../../security/user-authentication";

const router = Router();

export class PostRouter {
    private controller: PostApiController;
    private auth: UserAuthentication;

    constructor() {
        this.controller = new PostApiController();
        this.auth = new UserAuthentication();
    }

    get routes() {
        router.get("/posts", 
            this.controller.getAllPosts
        );

        router.get("/posts/:slug", 
            this.controller.getPost
        );

        router.post("/posts",
            this.auth.verifyHeaders,
            this.controller.addPost
        );

        router.patch("/posts/:slug", 
            this.auth.verifyHeaders, 
            this.controller.updatePost
        );

        router.delete("/posts/:slug",
            this.auth.verifyHeaders, 
            this.controller.deletePost
        );

        return router;
    }
}