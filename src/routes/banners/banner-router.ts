import { Router } from "express";

import { BannerApiController } from "./../../controllers";
import { UserAuthentication } from "./../../security/user-authentication";

const router = Router();

export class BannerRouter {
    private controller: BannerApiController;
    private auth: UserAuthentication;

    constructor() {
        this.controller = new BannerApiController();
        this.auth = new UserAuthentication();
    }

    get routes() {
        router.get("/banners", 
            this.controller.getBanners
        );

        router.get("/banners/:group", 
            this.controller.getBanners
        );

        router.post("/banners", 
            this.auth.verifyHeaders,
            this.controller.addBanner
        );

        router.patch("/banners/:_id", 
            this.auth.verifyHeaders,
            this.controller.updateBanner
        );

        router.delete("/banners/:_id", 
            this.auth.verifyHeaders,
            this.controller.deleteBanner
        );

        return router;
    }
}