import { Request, Response } from "express";

import { MenuRepository } from "./../../repositories";
import { Menu } from "./../../database/models";

export class MenuApiController {

    async getMenus(req: Request, res: Response) {
        const repository = new MenuRepository();

        const docs = !!req.params.slug ? await repository.getAll(req.params.slug)
        : await repository.getAll();

        if (!docs) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            menus: docs
        });
    }

    async addMenu(req: Request, res: Response) {
        const repository = new MenuRepository();

        try {
            const doc = await repository.insert(new Menu(req.body));

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }

            doc.populate({ path: "menu.pages", select: "-content" }, (err, menu) => {
                return res.status(200).send({
                    menu: menu
                });
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async updateMenu(req: Request, res: Response) {
        const repository = new MenuRepository();

        try {
            const doc = await repository.update(req.params.slug, req.body);

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                menu: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async deleteMenu(req: Request, res: Response) {
        const repository = new MenuRepository();

        const doc = await repository.delete(req.params.slug);
        
        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            menu: doc
        });
    }

}