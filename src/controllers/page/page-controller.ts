import { Request, Response } from "express";

import { PageRepository } from "./../../repositories";
import { Page } from "./../../database/models";

export class PageApiController {

    async getAllPages(req: Request, res: Response) {
        const repository = new PageRepository();

        const docs = await repository.getAll();

        if (!docs) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            pages: docs
        });
    }

    async getPage(req: Request, res: Response) {
        const repository = new PageRepository();
        
        const doc = await repository.get(req.params.slug);

        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            page: doc
        });
    }

    async addPage(req: Request, res: Response) {
        const repository = new PageRepository();

        try {
            const doc = await repository.insert(new Page(req.body));

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                page: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async updatePage(req: Request, res: Response) {
        const repository = new PageRepository();

        try {
            const doc = await repository.update(req.params.slug, req.body);

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                page: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async deletePage(req: Request, res: Response) {
        const repository = new PageRepository();

        const doc = await repository.delete(req.params.slug);
        
        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            page: doc
        });
    }

}