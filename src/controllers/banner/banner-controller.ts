import { Request, Response } from "express";

import { BannerRepository } from "./../../repositories";
import { Banner } from "./../../database/models";

export class BannerApiController {

    async getBanners(req: Request, res: Response) {
        const repository = new BannerRepository();

        const docs = !!req.params.group ? await repository.getAll(req.params.group)
            : await repository.getAll();

        if (!docs) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            banners: docs
        });
    }

    async addBanner(req: Request, res: Response) {
        const repository = new BannerRepository();

        try {
            const doc = await repository.insert(new Banner(req.body));

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                banner: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async updateBanner(req: Request, res: Response) {
        const repository = new BannerRepository();

        try {
            const doc = await repository.update(req.params._id, req.body);

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                banner: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async deleteBanner(req: Request, res: Response) {
        const repository = new BannerRepository();

        const doc = await repository.delete(req.params._id);
        
        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            banner: doc
        });
    }

}