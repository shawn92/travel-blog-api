import { Request, Response } from "express";

import { PostRepository } from "./../../repositories";
import { Post } from "./../../database/models";

export class PostApiController {

    async getAllPosts(req: Request, res: Response) {
        const repository = new PostRepository();

        const docs = await repository.getAll(req.query);

        if (!docs) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            posts: docs
        });
    }

    async getPost(req: Request, res: Response) {
        const repository = new PostRepository();

        const doc = await repository.get(req.params.slug);

        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            post: doc
        });
    }

    async addPost(req: Request, res: Response) {
        const repository = new PostRepository();

        try {
            const doc = await repository.insert(new Post(req.body));

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                post: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async updatePost(req: Request, res: Response) {
        const repository = new PostRepository();

        try {
            const doc = await repository.update(req.params.slug, req.body);

            if (!doc) {
                return res.status(404).send({   
                    error: "NOT_FOUND"
                });
            }
            return res.status(200).send({
                post: doc
            });
        }
        catch(e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async deletePost(req: Request, res: Response) {
        const repository = new PostRepository();

        const doc = await repository.delete(req.params.slug);
        
        if (!doc) {
            return res.status(404).send({
                error: "NOT_FOUND"
            });
        }
        return res.status(200).send({
            post: doc
        });
    }

}