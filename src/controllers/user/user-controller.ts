import * as _ from "lodash";

import { Request, Response } from "express";

import { IUser, User } from "./../../database/models";
import { UserAuthentication } from "./../../security/user-authentication";

export class UserApiController {
    private auth: UserAuthentication;

    constructor() {
        this.auth = new UserAuthentication();
    }

    async createUser(req: Request, res: Response) {
        const auth = new UserAuthentication();

        try {
            let user = { 
                username: req.body.username, 
                password: req.body.password
            } as IUser;

            const authData = await auth.createUser(user);

            if (!!authData) {
                const data = _.pick(authData.user, ["_id", "username"]);
                return res.header("X-Auth", authData.token).send(data);
            }

            return res.status(403).send();
        }
        catch (e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async loginUser(req: Request, res: Response) {
        const auth = new UserAuthentication();

        try {
            let user = { 
                username: req.body.username, 
                password: req.body.password,
                tokens: []
            } as IUser;

            const authData = await auth.login(user);

            if (!!authData) {
                const data = _.pick(authData.user, ["_id", "username"]);
                return res.header("X-Auth", authData.token).send(data);
            }

            return res.status(403).send();
        }
        catch (e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }

    async logoutUser(req: Request, res: Response) {
        const auth = new UserAuthentication();

        try {
            const user = req.body.user;
            const token = req.body.token;

            const isLoggedOut = await auth.logout(user, token);

            if (isLoggedOut) {
                return res.status(200).send();
            }

            return res.status(400).send();
        }
        catch (e) {
            return res.status(400).send({
                error: "BAD_REQUEST",
                message: e
            });
        }
    }
}