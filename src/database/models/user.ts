import * as mongoose from "mongoose";
import { isEmail } from "validator";

export interface IUser extends mongoose.Document {
    username: string;
    password: string;
    tokens?: [
        { 
            access: string;
            token: string;
        }
    ];
};

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        minlength: 8
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
});

UserSchema.set("toJSON", {
    transform: (doc: IUser, obj: IUser) => {
        delete obj.__v;
        return obj;
    }
});
export const User = mongoose.model<IUser>("User", UserSchema, "users");