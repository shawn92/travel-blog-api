import * as mongoose from "mongoose";

export interface IPage extends mongoose.Document {
    _id: string;
    title: string;
    slug: string;
    content?: string;
    isEnabled: boolean;
};

const PageSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true,
        maxlength: 20,
        unique: true
    },
    slug: {
        type: String,
        required: true,
        unique: true 
    },
    content: {
        type: String
    },
    isEnabled: {
        type: Boolean,
        required: true,
        default: true
    }
});

PageSchema.set("toJSON", {
    transform: (doc: IPage, obj: IPage) => {
        delete obj.__v;
        return obj;
    }
});
export const Page = mongoose.model<IPage>("Page", PageSchema, "pages");