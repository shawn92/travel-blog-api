import * as mongoose from "mongoose";

export interface IPost extends mongoose.Document {
    _id: string;
    title: string;
    slug: string;
    content: string;
    tags?: string[];
    author: string;
    comments?: Comment[];
    createdAt: Date;
    updatedAt: Date;
};

const PostSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true,
        maxlength: 20,
        unique: true
    },
    slug: {
        type: String,
        required: true,
        unique: true
    },
    content: {
        type: String,
        required: true
    },
    tags: {
        type: [String]
    },
    author: {
        type: String,
        required: true
    }
}, { timestamps: true });

PostSchema.set("toJSON", {
    transform: (doc: IPost, obj: IPost) => {
        delete obj.__v;
        return obj;
    }
});

export const Post = mongoose.model<IPost>("Post", PostSchema, "posts");