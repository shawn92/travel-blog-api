import * as mongoose from "mongoose";

import { IPage } from "./page";

export interface IMenu extends mongoose.Document {
    _id: string;
    slug: string;
    pages?: IPage[];
};

const MenuSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true,
        unique: true
    },
    pages: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: "Page"
    }
});

const resolveRefs = function(next: Function) {
    this.populate({
        path: "pages",
        model: "Page",
        select: "-content"
    });
    next();
};

MenuSchema.pre("find", resolveRefs);
MenuSchema.pre("findOne", resolveRefs);
MenuSchema.pre("findOneAndUpdate", resolveRefs);
MenuSchema.pre("findOneAndRemove", resolveRefs);

MenuSchema.set("toJSON", {
    transform: (doc: IPage, obj: IPage) => {
        delete obj.__v;
        return obj;
    }
});
export const Menu = mongoose.model<IMenu>("Menu", MenuSchema, "menus");