import * as mongoose from "mongoose";

export interface IBanner extends mongoose.Document {
    _id: string;
    title: string;
    url: string;
    group: string;
    width: number;
    height: number;
};

const BannerSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true,
        maxlength: 20,
        unique: true
    },
    url: {
        type: String,
        required: true,
        unique: true
    },
    group: {
        type: String,
        required: true
    },
    width: {
        type: Number,
        required: true
    },
    height: {
        type: Number,
        required: true
    }
});

BannerSchema.set("toJSON", {
    transform: (doc: IBanner, obj: IBanner) => {
        delete obj.__v;
        return obj;
    }
});
export const Banner = mongoose.model<IBanner>("Banner", BannerSchema, "banners");
