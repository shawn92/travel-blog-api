import * as mongoose from "mongoose";

import { Request } from "express";

import { Repository } from "./../repository";
import { IPage, Page } from "./../../database/models";

export class PageRepository extends Repository<IPage> {
    constructor() {
        super(Page);
    }
}