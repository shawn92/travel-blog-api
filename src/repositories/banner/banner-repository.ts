import * as mongoose from "mongoose";

import { Request } from "express";

import { Repository } from "./../repository";
import { IBanner, Banner } from "./../../database/models";
import { mapToKeys } from "./../../utils/utils";

export class BannerRepository extends Repository<IBanner> {
    constructor() {
        super(Banner);
    }

    // e.g. /api/banners/about
    async getAll(key?: string): Promise<mongoose.Document[]> {
        const docs = !!key ? await this.collection.find({group: key})
            : await this.collection.find();

        return mapToKeys(docs, "group");
    };
    
    async update(key: string, body: IBanner): Promise<mongoose.Document> {
        return await this.collection.findOneAndUpdate({_id: key},
            { $set: body },
            { runValidators: true, new: true }
        );
    }

    async delete(key: string): Promise<mongoose.Document> {
        return await this.collection.findOneAndRemove({_id: key});
    }
}