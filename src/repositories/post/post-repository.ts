import * as mongoose from "mongoose";
import * as _ from "lodash";

import { Request } from "express";

import { Repository } from "./../repository";
import { IPost, Post } from "./../../database/models";

export class PostRepository extends Repository<IPost> {
    constructor() {
        super(Post);
    }

    // e.g. /api/posts?tag=featured
    async getAll(key?: { tag: string }): Promise<mongoose.Document[]> {
        const docs = await this.collection.find();

        return docs.filter((doc: IPost) => {
            if (!!key.tag) {
                return _.find(doc.tags, tag => key.tag === tag);
            }
            return doc;
        });
    }
}