import * as mongoose from "mongoose";

import { Request } from "express";

import { Repository } from "./../repository";
import { IMenu, Menu } from "./../../database/models";
import { mapToKeys, mapToObject } from "./../../utils/utils";

export class MenuRepository extends Repository<IMenu> {
    constructor() {
        super(Menu);
    }

    // e.g. /api/menus/header
    async getAll(key?: string): Promise<mongoose.Document[]> {
        const docs = !!key ? await this.collection.find({slug: key})
            : await this.collection.find();
        return mapToObject(docs, "slug");
    };
}