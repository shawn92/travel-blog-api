import * as mongoose from "mongoose";
import * as _ from "lodash";

import { Request, Response, NextFunction } from "express";
import { compare, genSaltSync, hashSync } from "bcryptjs";
import { sign, verify } from "jsonwebtoken";

import { IUser, User } from "./../database/models";

export interface IAuthentication {
    createUser(user: IUser): Promise<{user: IUser, token: string}>;
    login(user: IUser): Promise<{user: IUser, token: string}>;
    logout(user: IUser, token: string): Promise<boolean>;
    verifyHeaders(req: Request, res: Response, next: Function): Promise<void | Response>;
}

export interface AuthToken {
    access: string;
    token: string;
}

export interface AuthenticatedUser {
    user: IUser;
    isAuthenticated: boolean;
}

export class UserAuthentication implements IAuthentication {
    async createUser(user: IUser): Promise<{user: IUser, token: string}> {
        const doc = new User({
            username: user.username, 
            password: this.hashPassword(user.password)
        });

        const createdUser = await doc.save();

        if (createdUser) {
            const authToken = await this.generateAuthToken(createdUser);

            if (!!authToken) {
                createdUser.tokens.push(authToken);

                const authUser = await createdUser.save();
                return { user: authUser, token: authToken.token };
            }
        }

        return undefined;
    }

    async login(user: IUser): Promise<{user: IUser, token: string}> {
        const authUser = await this.authenticate(user);

        if (!!authUser) {
            const authToken = await this.generateAuthToken(authUser);

            if (!!authToken) {
                user.tokens.push(authToken);

                const updatedUser = await User.findOneAndUpdate({username: user.username},
                    { $set: { tokens: user.tokens } },
                    { runValidators: true, new: true }
                );

                return { user: updatedUser, token: authToken.token };
            }
        }
    }

    async logout(user: IUser, token: string): Promise<boolean> {
        const loggedOutUser = await User.findByIdAndUpdate(user._id, {
            $pull: {
                tokens: {
                    token: token
                }
            }
        }).exec();

        return !!loggedOutUser;
    }

    async verifyHeaders(req: Request, res: Response, next: NextFunction): Promise<void | Response> {
        const token = req.header("X-Auth");

        try {
            const decoded: any = verify(token, process.env.JWT_SECRET, { algorithms: ["HS256"] });
            const authToken = { decoded: decoded.id, access: "auth" };

            if (!!authToken) {
                const user = await User.findOne({ 
                    _id: authToken.decoded, 
                    "tokens.access": authToken.access
                });

                if (!user) {
                    return res.status(401).send();
                }

                return next();
            }

            return res.status(401).send();
        }
        catch (e) {
            return res.status(401).send();
        }
    }

    private async authenticate(user: IUser): Promise<IUser> {
        const authUser = await User.findOne({username: user.username});

        if (!authUser) {
            return undefined;
        }

        const isAuthenticated = await compare(user.password, authUser.password);
        return isAuthenticated ? authUser : undefined;
    }

    private async generateAuthToken(user: IUser): Promise<AuthToken> {
        const access = "auth";
        const token = sign({ id: user._id, access }, process.env.JWT_SECRET, { algorithm: "HS256" });
        
        const authToken = {
            access: access,
            token: token
        };
    
        return authToken;
    }

    private hashPassword(password: string): string {
        const salt = genSaltSync();
        return hashSync(password, salt);
    }
}